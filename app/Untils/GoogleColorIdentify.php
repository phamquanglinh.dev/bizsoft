<?php

namespace App\Untils;

class GoogleColorIdentify
{
    public static function getColor($colorCode): string
    {
        return match ($colorCode) {
            0, "0", 7, "7" => '#039be5',
            1, "1" => '##7986cb',
            2, "2" => '#33b679',
            3, '3' => '#8e24aa',
            4, "4" => '#e67c73',
            5, '5' => '#f6c026',
            6, '6' => '#f5511d',
            8, "8" => '#616161',
            9, "9" => '#3f51b5',
            10, "10" => '#0b8043',
            11, "11" => '#d60000',
        };
    }

    public static function getIdentify($color): string|int
    {
        return match ($color) {
            '#7986cb' => 1,
            '#33b679' => 2,
            '#8e24aa' => 3,
            '#e67c73' => 4,
            '#f6c026' => 5,
            '#f5511d' => 6,
            '#616161' => 8,
            '#3f51b5' => 9,
            '#0b8043' => 10,
            '#d60000' => 11,
            default => 0,
        };
    }
}
