<?php

namespace App\Repositories\Eloquent;

use App\Models\ConnectToken;
use App\Repositories\ConnectTokenRepositoryInterface;
use TimWassenburg\RepositoryGenerator\Repository\BaseRepository;

/**
 * Class ConnectTokenRepository.
 */
class ConnectTokenRepository extends BaseRepository implements ConnectTokenRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param ConnectToken $model
     */
    public function __construct(ConnectToken $model)
    {
        parent::__construct($model);
    }

    public function createOrUpdate(int $user_id, array $data): void
    {
        if ($oldConnection = $this->model->newModelQuery()->where("user_id", $user_id)->first()) {
            $oldConnection->update($data);
        } else {
            $data["user_id"]=$user_id;
            $this->create($data);
        }
    }
}
