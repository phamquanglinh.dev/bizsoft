<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Repositories\Eloquent\ConnectTokenRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Untils\GoogleColorIdentify;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class GoogleCalendarController extends GoogleClientController
{
    public function __construct(
        private readonly ConnectTokenRepository $connectTokenRepository,
        private readonly UserRepository         $userRepository
    )
    {
        parent::__construct($connectTokenRepository);
    }

    const BASE_API = "https://www.googleapis.com/calendar/v3";
    const CALENDAR_LIST = self::BASE_API . "/users/me/calendarList";

    function authAction(Request $request): JsonResponse
    {
        return $this->getLoginUrl($request["scope"] ?? null, $callback = $request["callback"] ?? null);
    }

    function getToken(Request $request): JsonResponse
    {
        $user = $request->user();
        return $this->getTokenByCode($user["id"], $request["code"] ?? null);
    }

    function getCalendarListAction(Request $request): PromiseInterface|Response
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->getUserWithConnection($request->user()->id);

        $this->updateToken($user);
        return Http::withToken($user->getToken())->get(self::CALENDAR_LIST);
    }

    public function getCalendarEventsByCalendarIdAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->getUserWithConnection($request->user()->id);
        $this->updateToken($user);
        $url = self::BASE_API . "/calendars/" . $request["calendarId"] . "/events";
        $url = str_replace("#", "%23", $url);
        $response = Http::withToken($user->getToken())->get($url);
        $bodyResponse = json_decode($response->body());
        $items = Collection::make($bodyResponse);
        $items = Collection::make($items["items"] ?? []);
        return ($items->map(function ($item) {
            $item = (array)$item;
            $start = (array)$item["start"];
            $end = (array)$item["end"];
            return
                [
                    'id' => $item['id'],
                    'title' => $item["summary"] ?? 'Không có tiêu đề',
                    'start' => $start["dateTime"] ?? $start["date"] ?? Carbon::now()->toDateTimeString(),
                    'end' => $end["dateTime"] ?? $end["date"] ?? Carbon::now()->toDateTimeString(),
                    'backgroundColor' => GoogleColorIdentify::getColor($item["colorId"] ?? 0)
                ];
        }));
    }

    public function createCalendarEventByCalendarIdAction(Request $request): mixed
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->getUserWithConnection($request->user()->id);
        $this->updateToken($user);
        $url = self::BASE_API . "/calendars/" . $request["calendarId"] . "/events";
        $requestBody = [
            'start' => [
                'dateTime' => Carbon::parse($request["start"])
            ],
            'end' => [
                'dateTime' => Carbon::parse($request["end"])
            ],
//            'colorId' => GoogleColorIdentify::getIdentify($request["color"] ?? 'unknown'),
            'colorId' => rand(1, 11),
            'summary' => $request["summary"] ?? $request["title"] ?? null
        ];
        $response = Http::withToken($user->getToken())->withBody(json_encode($requestBody))->post($url);
        return $response->body();
    }
}
