<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Eloquent\ConnectTokenRepository;
use Google\Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

abstract class GoogleClientController extends Controller
{
    protected \Google_Client $client;

    /**
     * @throws Exception
     */
    public function __construct(
        private readonly ConnectTokenRepository $connectTokenRepository
    )
    {
        $client = new \Google_Client();
        $client->setApplicationName('V6');
        $client->setAuthConfig(config('google-api.client_path'));
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setPrompt("consent");
        $this->client = $client;
    }

    public function getLoginUrl($scope = null, $callback = null): JsonResponse
    {
//        $this->client->setRedirectUri($callback);
        if ($scope) {
            return response()->json(['url' => $this->client->createAuthUrl(scope: $scope)]);
        }
        return response()->json(['message' => "Lỗi"]);
    }

    public function getTokenByCode(int $user, string $code): JsonResponse
    {
//        $this->client->setRedirectUri();
        $accessToken = $this->client->fetchAccessTokenWithAuthCode($code);
        if (array_key_exists('error', $accessToken)) {
            return response()->json(['error' => $accessToken], 500);
        }
        $this->connectTokenRepository->createOrUpdate(user_id: $user, data: $accessToken);
        return response()->json(['token' => $accessToken]);
    }

    public function updateToken(User $user)
    {
        $connectToken = $user->ConnectToken()->first();

        $response = Http::withQueryParameters(["access_token" => $connectToken["access_token"]])->get("https://oauth2.googleapis.com/tokeninfo");
        if ($response->status() != 200) {
            $accessToken = $this->client->fetchAccessTokenWithRefreshToken($connectToken["refresh_token"]);
            if (array_key_exists('error', $accessToken)) {
                return response()->json(['error' => $accessToken], 500);
            }
            $this->connectTokenRepository->createOrUpdate(user_id: $user["id"], data: $accessToken);
        }


    }


}
